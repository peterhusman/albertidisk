using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace AlbertiEncodingDisk
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont font;
        string value;
        KeyboardState keyboard;
        KeyboardState oldKeyboard;
        int i = 0;
        //temp
        int h;
        bool wrong;

        float letterAngle = MathHelper.ToRadians(360f / 26f);

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }



        string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";


        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            font = Content.Load<SpriteFont>("Text");
            value = ScrambledAlphabet(10);
            // TODO: use this.Content to load your game content here
        }


        public string ScrambledAlphabet(int seed)
        {

            List<int> usedInt = new List<int>();
            List<char> scrambled = new List<char>();
            Random scramble = new Random(seed);
            bool addToUsedInt = true;
            while (usedInt.Count() < 26)
            {
                int aIndex = scramble.Next(0, 26);
                foreach (int i in usedInt)
                {
                    if (aIndex == i)
                    {
                        addToUsedInt = false;
                        break;
                    }
                    else
                    {
                        addToUsedInt = true;
                    }
                }
                if (addToUsedInt)
                {
                    usedInt.Add(aIndex);
                }
            }
            for (int i = 0; i < 26; i++)
            {
                scrambled.Add(alphabet[usedInt[i]]);
            }

            //tempcode
            foreach (char c in alphabet)
            {
                foreach (char g in scrambled.ToArray())
                {
                    if (c == g)
                    {
                        h++;
                    }
                }
                if (h >= 2)
                {
                    wrong = true;
                }
                h = 0;
            }

            return new String(scrambled.ToArray()) + "|" + scrambled.Count.ToString() + "|" + seed.ToString() + "|" + wrong.ToString();

        }
        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }
        int tempy = 0;
        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();
            //keyboard = Keyboard.GetState();
            //if (keyboard.IsKeyDown(Keys.Space) && oldKeyboard.IsKeyUp(Keys.Space))
            //{
            //    i++;
            //    value = ScrambledAlphabet(i);
            //}
            //oldKeyboard = keyboard;
            // TODO: Add your update logic here
            tempy++;
            value = ScrambledAlphabet(tempy);
            if (value.Contains("|True"))
            {

            }

            base.Update(gameTime);
        }
        
        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin();
            spriteBatch.DrawString(font, value, Vector2.Zero, Color.White);
            
            Vector2 screenCenter = new Vector2(GraphicsDevice.Viewport.Width / 2f, GraphicsDevice.Viewport.Height / 2f);

            float radius = 100f;
            //Test code
            for (int i = 0; i < 26; i++)
            {
                //Vector2 circlePoint = new Vector2((float)Math.Cos(letterAngle * i), (float)Math.Sin(letterAngle * i)) * 100f;
                float x = (float)Math.Cos((letterAngle * i) - MathHelper.PiOver2);
                float y = (float)Math.Sin((letterAngle * i) - MathHelper.PiOver2);

                Vector2 albertiPoint = new Vector2(x, y) * radius;
                Vector2 alphabetPoint = new Vector2(x, y) * radius * 0.8f;

                spriteBatch.DrawString(font, value[i].ToString(), screenCenter + albertiPoint, Color.White, letterAngle * i, font.MeasureString(value[i].ToString()) / 2f, Vector2.One, SpriteEffects.None, 0);
                spriteBatch.DrawString(font, alphabet[i].ToString(), screenCenter + alphabetPoint, Color.Red, letterAngle * i, font.MeasureString(value[i].ToString()) / 2f, Vector2.One, SpriteEffects.None, 0);
                
            }

            spriteBatch.End();


            base.Draw(gameTime);
        }
    }
}
