﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Glib.XNA.SpriteLib;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace AlbertiEncodingDisk
{
    public class PredicatedVisibilitySprite : Sprite
    {
        public Func<Boolean> ShouldBeVisible { get; set; }
        public Action<Boolean> SetVisibility { get; set; }

        public PredicatedVisibilitySprite(Texture2D texture, Vector2 pos, SpriteBatch sb, Func<Boolean> visPred, Action<Boolean> setPred)
            : base(texture, pos, sb)
        {
            ShouldBeVisible = visPred;
            SetVisibility = setPred;
        }

        public override bool Visible
        {
            get
            {
                return ShouldBeVisible();
            }

            set
            {
                SetVisibility(value);
            }
        }
    }
}
