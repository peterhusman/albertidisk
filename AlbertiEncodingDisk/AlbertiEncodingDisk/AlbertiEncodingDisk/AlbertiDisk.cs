﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Glib.XNA;
using Glib.XNA.SpriteLib;
using Microsoft.Xna.Framework.Graphics;


namespace AlbertiEncodingDisk
{
    class AlbertiDisk : Glib.XNA.SpriteLib.Sprite
    {
        float _radius = 0f;
        string _diskString;
        TimeSpan _rotTime = new TimeSpan();
        TimeSpan _rotMaxT = new TimeSpan(0, 0, 0, 0, 500);
        List<RotateDirection> _rotateQueue = new List<RotateDirection>();
        float _letterAngle = MathHelper.ToRadians(360f) / 26f;
        Texture2D _texturet;
        SpriteFont _font;
        float _rot = 0f;
       float _rotRat = 0f;
        string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        int _oldSeed;



        public AlbertiDisk(Vector2 position, SpriteBatch spriteBatch, float radius, Color textColor, int seed, SpriteFont font)
            : base(null, position, spriteBatch)
        {
            _radius = radius;
            TextColor = textColor;
            _diskString = getScrambledAlphabetString(seed);
            Seed = seed;
            _font = font;
            _oldSeed = Seed * Seed;
        }


        public void Rotate(RotateDirection rotateDir)
        {
            _rotateQueue.Add(rotateDir);
        }

        public string getScrambledAlphabetString(int seed)
        {
            List<int> usedInt = new List<int>();
            List<char> scrambled = new List<char>();
            Random scramble = new Random(seed);
            bool addToUsedInt = true;
            while (usedInt.Count() < 26)
            {
                int aIndex = scramble.Next(0, 26);
                foreach (int i in usedInt)
                {
                    if (aIndex == i)
                    {
                        addToUsedInt = false;
                        break;
                    }
                    else
                    {
                        addToUsedInt = true;
                    }
                }
                if (addToUsedInt)
                {
                    usedInt.Add(aIndex);
                }
            }
            for (int i = 0; i < 26; i++)
            {
                scrambled.Add(alphabet[usedInt[i]]);
            }

            return new String(scrambled.ToArray());

        }



        /// <summary>
        /// Returns a dictionary that has the keys and values of the alphabet-crossover swapped.
        /// </summary>
        /// <param name="alphabetCrossover">The output of the AlphabetCrossover function</param>
        /// <returns></returns>
        public Dictionary<char, char> AlphabetCrossoverInverted(Dictionary<char, char> alphabetCrossover)
        {
            Dictionary<char, char> ACI = new Dictionary<char, char>();
            foreach (char c in alphabetCrossover.Keys)
            {
                ACI.Add(alphabetCrossover[c], c);
            }
            return ACI;
        }

        public Dictionary<char, char> AlphabetCrossover(string scrambledAlphabet)
        {

            Dictionary<char, char> charchar = new Dictionary<char, char>();
            for (int i = 0; i < 26; i++)
            {
                charchar.Add(alphabet[i], scrambledAlphabet[i]);

            }

            return charchar;
        }



        public string EncodedText(Dictionary<char, char> alphabetCrossover, string plaintext)
        {
            StringBuilder encodedText = new StringBuilder();
            plaintext = plaintext.ToUpper();

            int index = 0;
            foreach (char c in plaintext)
            {
                if (alphabet.Contains(c))
                {
                    encodedText.Append(alphabetCrossover[c]);
                }
                index++;
            }
            return encodedText.ToString();
        }




        public string DecodedText(Dictionary<char, char> alphabetCrossoverInverted, string ciphertext)
        {
            
            StringBuilder decodedText = new StringBuilder();
            ciphertext = ciphertext.ToUpper();
            int index = 0;
            foreach (char c in ciphertext)
            {
                if (alphabet.Contains(c))
                {
                    decodedText.Append(alphabetCrossoverInverted[c]);
                }
                index++;
            }
            return decodedText.ToString();
        }



        public override Texture2D Texture
        {
            get
            {
                return _texturet; //throw new Exception("Uhm... there is no texture...");
            }
            /*private*/
            set
            {
                base.Texture = value;
                _texturet = value;
            }
        }

        public string DiskText
        {
            get { return _diskString; }
            set { /*_diskString = value;*/ }
        }

        public Color TextColor { get; set; }


        public float Radius
        {
            get { return _radius; }
            set
            {
                if (value < 100)
                {
                    throw new Exception("Sorry! That radius is unreasonable. YOU try drawing 26 gigantic letters around a 100 pixel radius circle!");
                }

                _radius = value;
            }
        }

        public int Seed { get; set; }

        public void Draw(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < 26; i++)
            {
                Vector2 circlePoint = new Vector2((float)Math.Cos(_letterAngle * i), (float)Math.Sin(_letterAngle * i)) * 100f;
                float x = (float)Math.Cos((_letterAngle * i) - MathHelper.PiOver2);
                float y = (float)Math.Sin((_letterAngle * i) - MathHelper.PiOver2);
                float ax = (float)Math.Cos((_letterAngle * i) - MathHelper.PiOver2 + _rot);
                float ay = (float)Math.Sin((_letterAngle * i) - MathHelper.PiOver2 + _rot);
                Vector2 albertiPoint = new Vector2(ax, ay) * _radius;
                Vector2 alphabetPoint = new Vector2(x, y) * _radius * 0.8f;

                spriteBatch.DrawString(_font, _diskString[i].ToString(), base.Position + albertiPoint, Color.White, _letterAngle * i + _rot, _font.MeasureString(_diskString[i].ToString()) / 2f, Vector2.One, SpriteEffects.None, 0);
                spriteBatch.DrawString(_font, alphabet[i].ToString(), base.Position + alphabetPoint, Color.Red, _letterAngle * i, _font.MeasureString(_diskString[i].ToString()) / 2f, Vector2.One, SpriteEffects.None, 0);
            }
        }
        
        public void Update(GameTime gameTime)
        {
            base.Update();
            if (_oldSeed != Seed)
            {
                _diskString = getScrambledAlphabetString(Seed);
                _oldSeed = Seed;
            }

            if (_rotateQueue.Count > 0 && _rotTime >= _rotMaxT)
            {
                _rotTime = TimeSpan.Zero;
                _rot = 0f;
                if (_rotateQueue[0] == RotateDirection.Left)
                {
                    _diskString = _diskString + _diskString[0];
                    _diskString = _diskString.Remove(0, 1);
                }
                else
                {
                    _diskString = _diskString[25] + _diskString;
                    _diskString = _diskString.Remove(26);
                }
                _rotateQueue.Remove(_rotateQueue[0]);
            }
            
            if (_rotateQueue.Count > 0)
            {
                _rotTime += gameTime.ElapsedGameTime;


                if (_rotateQueue[0] == RotateDirection.Left)
                {
                    _rot = SmoothStep((float)_rotTime.Ticks / (float)_rotMaxT.Ticks, 0, (float)Math.PI * -2 / 26f);
                    //Do magic with sin, cosin, pi, and tau
                }
                if (_rotateQueue[0] == RotateDirection.Right)
                {
                    _rot = SmoothStep((float)_rotTime.Ticks / (float)_rotMaxT.Ticks, 0f, (float)Math.PI * 2 / 26f);
                    //Do the same magic, but in the other direction
                }
            }

            for (int i = 0; i < 26; i++)
            {

            }


        }




        private float SmoothStep(float zeroToOneVal, float initRot, float endRot)
        {
            zeroToOneVal = MathHelper.Clamp(zeroToOneVal, 0, 1);
            return (endRot - initRot) * zeroToOneVal + initRot;
        }
    }
}
