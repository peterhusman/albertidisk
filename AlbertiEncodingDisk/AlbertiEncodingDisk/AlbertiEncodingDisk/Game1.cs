using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Text;
using System.Net;
using Glib;
using Glib.XNA;
using Glib.XNA.SpriteLib;


namespace AlbertiEncodingDisk
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont font;
        TextBoxSprite messageInput;
        TextBoxSprite enmessageInput;
        PredicatedVisibilitySprite enmessageInputBorder;
        PredicatedVisibilitySprite messageInputBorder;
        KeyboardState keyboard;
        KeyboardState oldKeyboard;
        AlbertiDisk disk;


        string message = "YOUR MESSAGE";
        
        string enMessage;
        float letterAngle = MathHelper.ToRadians(360f / 26f);

        public Game1()
        {

            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            Glib.XNA.InputLib.InputManagerComponent component = new Glib.XNA.InputLib.InputManagerComponent(this);
            
            this.Components.Add(component);



            base.Initialize();
        }

        void messageInput_TextSubmitted(object sender, EventArgs e)
        {
            message = messageInput.Text.ToUpper().Replace(" ", "");
            enMessage = disk.EncodedText(disk.AlphabetCrossover(disk.DiskText), message);
        }



        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            graphics.PreferredBackBufferHeight = 800;
            graphics.PreferredBackBufferWidth = 800;
            IsMouseVisible = true;
            font = Content.Load<SpriteFont>("Text");
            disk = new AlbertiDisk(new Vector2(500, 300), spriteBatch, 100f, Color.White, 10, font);
            enMessage = disk.EncodedText(disk.AlphabetCrossover(disk.getScrambledAlphabetString(disk.Seed)), message);
            messageInput = new TextBoxSprite(new Vector2(6f, 200f), spriteBatch, font);
            messageInput.Width = 220;
            messageInput.Height = font.MeasureString("ABESWAG").Y;
            messageInput.TextSubmitted += new EventHandler(messageInput_TextSubmitted);
            messageInput.KeyPressDelay = new TimeSpan(0, 0, 0, 0, 300);
            enmessageInput = new TextBoxSprite(new Vector2(6f, 350f), spriteBatch, font);
            enmessageInput.Width = 200;
            enmessageInput.Height = font.MeasureString("ABESWAG").Y;
            enmessageInput.TextSubmitted += new EventHandler(enmessageInput_TextSubmitted);
            enmessageInput.KeyPressDelay = new TimeSpan(0, 0, 0, 0, 300);
            messageInput.Focused = false;
            enmessageInput.Focused = true;

            TextureFactory factory = new TextureFactory(GraphicsDevice);

            enmessageInputBorder = new PredicatedVisibilitySprite(factory.CreateHollowRectangle((int)enmessageInput.Width + 2, (int)enmessageInput.Height + 2, Color.Red), enmessageInput.Position - Vector2.One, spriteBatch, () => enmessageInput.Focused, (vis) => enmessageInput.Focused = vis);
            messageInputBorder = new PredicatedVisibilitySprite(factory.CreateHollowRectangle((int)messageInput.Width + 2, (int)messageInput.Height + 2, Color.Red), messageInput.Position - Vector2.One, spriteBatch, () => messageInput.Focused, (vis) => messageInput.Focused = vis);
            
            //disk.Texture = Content.Load<Texture2D>("StansInconspicuosEncodedImage");
            // TODO: use this.Content to load your game content here
            
            
        }

        void enmessageInput_TextSubmitted(object sender, EventArgs e)
        {
            enMessage = enmessageInput.Text.ToUpper().Replace(" ", "");
        }




        

        protected override void UnloadContent()
        {

        }
        int keyVal = 0;
        

        protected override void Update(GameTime gameTime)
        {
            
            disk.Update(gameTime);
            messageInput.Update(gameTime);
            enmessageInput.Update(gameTime);


            if (messageInput.ClickCheck())
            {
                messageInput.Focused = true;
                enmessageInput.Focused = false;

            }
            if (enmessageInput.ClickCheck())
            {
                enmessageInput.Focused = true;
                messageInput.Focused = false;

            }

            keyboard = Keyboard.GetState();
            if (keyboard.IsKeyDown(Keys.Right) && oldKeyboard.IsKeyUp(Keys.Right))
            {
                disk.Rotate(RotateDirection.Right);
            }
            if (keyboard.IsKeyDown(Keys.Left) && oldKeyboard.IsKeyUp(Keys.Left))
            {
               disk.Rotate(RotateDirection.Left);
            }
            if (keyboard.IsKeyDown(Keys.Space) && oldKeyboard.IsKeyUp(Keys.Space))
            {
                disk.Seed++;
                
            }
            if (keyboard.IsKeyDown(Keys.RightAlt))
            {
                disk.Seed++;
                
            }
            if (keyboard.IsKeyDown(Keys.F1))
            {
                messageInput.Focused = false;
                enmessageInput.Focused = false;
            }
            if(keyboard.IsKeyDown(Keys.OemPeriod))
            {
                disk.Seed = 0;
            }
            if (keyboard.IsKeyDown(Keys.OemQuotes) && oldKeyboard.IsKeyUp(Keys.OemQuotes))
            {
                enMessage = disk.EncodedText(disk.AlphabetCrossover(disk.DiskText), message);
            }
            if (keyboard.GetPressedKeys().Length > 0)
            {
                if (oldKeyboard.GetPressedKeys().Length == 0 && (int)(keyboard.GetPressedKeys()[0]) <= 105 && (int)(keyboard.GetPressedKeys()[0]) >= 96)
                {
                    keyVal = ((int)(keyboard.GetPressedKeys()[0])) - 96;
                    disk.Seed *= 10;
                    disk.Seed += keyVal;
                }
            }
            oldKeyboard = keyboard;

            messageInput.Update(gameTime);
            
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            
            GraphicsDevice.Clear(Color.CornflowerBlue);


            
            spriteBatch.Begin();
            disk.Draw(spriteBatch);
            spriteBatch.DrawString(font, disk.DiskText + '|' + disk.Seed.ToString(), Vector2.Zero, Color.White);


            

            Vector2 screenCenter = new Vector2(GraphicsDevice.Viewport.Width / 2f, GraphicsDevice.Viewport.Height / 2f);
            spriteBatch.DrawString(font, "Encoded Message:\n" + enMessage, new Vector2(0f, 375f), Color.Red);
            spriteBatch.DrawString(font, "To Decode:\n", new Vector2(0f, 325f), Color.Red);
            spriteBatch.DrawString(font, "Plaintext Message:\n" + disk.DecodedText(disk.AlphabetCrossoverInverted(disk.AlphabetCrossover(disk.DiskText)), enMessage), new Vector2(0f, 250f), Color.Black);
            spriteBatch.DrawString(font, "To Encode:\n", new Vector2(0f, 175f), Color.Black);

            //Test code
            for (int i = 0; i < 26; i++)
            {
                //Vector2 circlePoint = new Vector2((float)Math.Cos(letterAngle * i), (float)Math.Sin(letterAngle * i)) * 100f;
                //float x = (float)Math.Cos((letterAngle * i) - MathHelper.PiOver2);
                //float y = (float)Math.Sin((letterAngle * i) - MathHelper.PiOver2);

                //Vector2 albertiPoint = new Vector2(x, y) * radius;
                //Vector2 alphabetPoint = new Vector2(x, y) * radius * 0.8f;

                //spriteBatch.DrawString(font, "Hi Stan", new Vector2(0f, 100f), Color.Yellow);
               
                //spriteBatch.DrawString(font, value[i].ToString(), screenCenter + albertiPoint, Color.White, letterAngle * i, font.MeasureString(value[i].ToString()) / 2f, Vector2.One, SpriteEffects.None, 0);
                //spriteBatch.DrawString(font, alphabet[i].ToString(), screenCenter + alphabetPoint, Color.Red, letterAngle * i, font.MeasureString(value[i].ToString()) / 2f, Vector2.One, SpriteEffects.None, 0);

            }

            messageInput.DrawNonAuto();
            messageInputBorder.DrawNonAuto();
            enmessageInput.DrawNonAuto();
            enmessageInputBorder.DrawNonAuto();
            spriteBatch.End();
            
            base.Draw(gameTime);
        }
    }
}
